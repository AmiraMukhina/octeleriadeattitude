// animation
function onEntry(entry) {
    entry.forEach(change => {
        if (change.isIntersecting) {
            change
                .target
                .classList
                .add('element-show');
        }
    });
}

let options = {
    threshold: [0.5]
};
let observer = new IntersectionObserver(onEntry, options);
let elements = document.querySelectorAll('.element-animation');

for (let elm of elements) {
    observer.observe(elm);
}




const btnLoc = document.getElementById('info-btn-loc');
const btnTime = document.getElementById('info-btn-time');
const hiddenLoc = document.getElementById('hidden-loc');
const hiddenTime = document.getElementById('hidden-time');


btnLoc.addEventListener("click", function () {
    btnLoc.classList.toggle('btn-active');
    hiddenLoc.classList.toggle('info-active');
});

btnTime.addEventListener("click", function () {
    btnTime.classList.toggle('btn-active');
    hiddenTime.classList.toggle('info-active');
});


// BURGER 
const menuBtn = document.getElementById('menu-btn');
const menu = document.getElementById('nav');
const toggleMenu = function () {
    menu.classList.toggle('active-menu');
}

menuBtn.addEventListener("click", function (e) {
    menuBtn.classList.toggle('active-menu-btn');
    e.stopPropagation();
    toggleMenu();
});

